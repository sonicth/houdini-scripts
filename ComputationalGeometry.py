import hou
import bisect

def getXY2D(points):
	positions = []
	for pt in getXZProjection(points):
		# -z -> y, x is the same
		positions.append(hou.Vector2(pt.x(), -pt.z()))
	return positions

#for pt in geo.points():
#    print ("point position {}".format(pt.position() ))
#    p = pt.position()
#    pt.setPosition( hou.Vector3(p.x(), 0, p.z()) )
def getXZProjection(points):
	positions = []
	for pt in points:
		p = pt.position()
		positions.append(hou.Vector3(p.x(), 0, p.z()))
	return positions

def getCentre(positions):
	centre = hou.Vector3(0, 0, 0)
	for p in positions:
		centre += p
	centre /= len(positions)
	return centre

def rotateForward(list, n = 1):
	if (n >= len(list)):
		print("error: rotation parameter too big ({0}), should be smaller than list length ({1})!".format(n, len(list)))
	return list[n:] + list[:n]
		
# directions from current to next vertices
def getDirections(positions):
	n = len(positions)
	prev = positions[n-1]
	dirs = []
	for curr in positions:
		dirs.append(curr - prev)
		prev = curr
	return rotateForward(dirs)

def sortPolar(positions, centre, target):
	n = len(positions)

	# direcitons from centre
	cdirs = []
	for p_global in positions:
		p = p_global - centre
		cdirs.append(p)
	cdirs = getDirections(positions)

	crosses = []
	for d in cdirs:
		crossv = hou.Vector3.cross(target.normalized(), d.normalized())
		crosses.append(crossv.length())
	
	zipped = zip(range(len(positions)), positions, crosses)
	# sort by crosses
	zipped.sort(key=lambda z: z[2])

	#TODO sort using crosses
	for c in zipped:
		print ("joined {0}".format(c))
	return zipped

def polarDemo(geo):
	positions = getXZProjection(geo.points())
	centre = getCentre(positions)
	sorted_positions = sortPolar(positions, centre, positions[0])

	# Create the "Cd" point attribute value, giving it a default value of white
	# (1, 1, 1), and store the returned hou.Attrib object.
	cd = geo.addAttrib(hou.attribType.Point, "Cd", (1.0, 1.0, 1.0))
	color = hou.Color()
	

	# TODO visualise positions
	# create primitive group
	g = geo.createPrimGroup("VizPolarDirections", True)
	i = 0.0
	for sp in sorted_positions:
		linepoly = geo.createPolygon()
		#addpoints
		value = i / len(sorted_positions)
		color.setHSV((value * 256, 1, 1))
		pt1 = geo.createPoint(); pt1.setPosition(centre);	pt1.setAttribValue(cd, color.rgb())
		pt2 = geo.createPoint(); pt2.setPosition(sp[1]);	pt2.setAttribValue(cd, color.rgb())
		linepoly.addVertex(pt1)
		linepoly.addVertex(pt2)
		g.add(linepoly)
		i += 1

################################################################
# segments
################################################################
# 2D line segment
class Segment2:
	def __init__(self, points_):
		# two end points check
		if len(points_) != 2:
			raise ValueError("Expected list with element length 2, got {0}".format(len(points_)))

		# must NOT be vertical
		x1 = points_[0][0]
		x2 = points_[1][0]
		if x1 == x2:
			raise ValueError("Expected non-vertical segment data (x values are equal)")
		
		# check x order
		if x1 > x2:
			# # raise ValueError("Segment is not ordered, first point x {0} should be smaller than second point x {1}".format(x1, x2))
			# re-order x
			#TODO make generic by dealing non-array/list cases
			self.points = [points_[1], points_[0]]
			print("Segment points re-ordered pt1 {0}, pt2 {1}".format(self.points[0], self.points[1]))
		else:
			self.points = points_

	def x1(self): return self.points[0][0]
	def x2(self): return self.points[1][0]
	def y1(self): return self.points[0][1]
	def y2(self): return self.points[1][1]

	def comparable(self, at_x):
		return self.x1() <= at_x and at_x <= self.x2()

	# get Y value at some position X
	def sampleAtX(self, x):
		if (x < self.x1()):
			raise ValueError("Samling below the segment at {0}".format(x))
		if (x > self.x2()):
			raise ValueError("Samling above the segment at {0}".format(x))

		# parametrise x within segment
		dx = self.x2() - self.x1()
		t = (x - self.x1()) / dx

		# compute y value
		dy = self.y2() - self.y1()
		y = self.y1() + t*dy

		return y
		
	def __eq__(self, other):
		return (
			self.x1() == other.x1() and
			self.x2() == other.x2() and
			self.y1() == other.y1() and
			self.y2() == other.y2() )

	def __repr__(self):
		return "Segment2{{{0}, {1}}}".format(self.points[0], self.points[1])

# select a sweepline of a segment
class SegmentCmp:
	def __init__(self, x_, segment_):
		#TODO test that segment is of type Segment2
		self.x = x_
		self.segment = segment_

	# 'above' relation between segments
	def __gt__(self, other_segment):
		s1y = self.segment.sampleAtX(self.x)
		s2y = other_segment.sampleAtX(self.x)
		return s1y > s2y

def segmentsIntersect(s1, s2, i, j):
	x_start = max(s1.x1(), s2.x1())
	x_end = min(s1.x2(), s2.x2())
	gt_start = SegmentCmp(x_start, s1) > s2
	gt_end = SegmentCmp(x_end, s1) > s2
	if gt_start != gt_end:
		print("segment {0} and {1} intersect!".format(i, j))
		return True
	else:
		return False

def segmentIntersectionsNaiveQuadratic(segs):
	i = 0
	for s1 in segs:
		j = 0
		for s2 in segs:
			if s1 != s2: #OR i != j
				if segmentsIntersect(s1, s2, i, j):
					return True
			else:
				print ("skilling self-intersections of {0},{1}".format(i, j))
			j += 1
		i+=1
	return False

class SweepLineStatus:
	def __init__(self, segs):
		self.segs = segs

		# construct event points...
		eventPtsFirsts = [(s.points[0], idx, 0) for idx,s in enumerate(segs)]
		eventPtsSeconds = [(s.points[1], idx, 1) for idx,s in enumerate(segs)]
		# combine 
		self.eventPts = eventPtsFirsts + eventPtsSeconds
		
		# put those with lower y coordinates first
		# TODO test
		self.eventPts.sort(key=lambda event: event[0][1])

		# put left covertical points before right ones
		# TODO test
		self.eventPts.sort(key=lambda event: event[2])

		# and sort by X values
		# NOTE previous things were sorted and would remain stable 
		self.eventPts.sort(key=lambda event: event[0][0])

		#position within the event points
		self.position = -1
		# current segments
		self.sweeplineStatus = []
		# print(self.eventPts)

	def insertSegment(self, segmentIdx):
		# sorted by height/y
		bisect.insort_left(self.sweeplineStatus, segmentIdx, key=lambda idx: self.segs[idx].y1())

	def removeSegment(self, segmentIdx):
		#TODO remove the segment
		pass

	def advanceSweepline(self):
		self.position += 1
		pt, idx, side = self.eventPts[self.position]
		if side == 0:
			# entering the 
			self.insertSegment(idx)
		else:
			#segment will be leaving
			self.removeSegment(idx)
		
		

def segmentIntersectionsSweep(segs):
	sweeper = SweepLineStatus(segs)


	return False

def segmentIntersections(segs):
	# return segmentIntersectionsNaiveQuadratic(segs)
	return segmentIntersectionsSweep(segs)

################################################################
################################################################
################################################################
if __name__ == "__main__":
	print("don't call me directly, I am a module!")

