##an example application
#
#import ConvexTest
#reload(ConvexTest)
#node = hou.pwd()
#geo = node.geometry()
#if ConvexTest.isConvex(geo):
#    print("is convex")
#else:
#    print("NOT convex!")

import hou
import math
import ComputationalGeometry as cg
reload(cg)

def getTurn(cross_value):
	if (cross_value > 0):
		return "Left"
	elif (cross_value < 0):
		return "Right"
	else:
		return "Centre"

def computeTurn(dirCurrent, dirNext):
	xvec = dirCurrent.cross(dirNext);
	xval = xvec[1]
	dot = dirCurrent.dot(dirNext)
	degrees = math.asin(xval) / math.pi * 180.0 
	is_sharp = dot >= 0
	angle_complete = degrees if is_sharp else 180.0  + (-degrees if degrees > 0 else degrees)
	#print("cross value {0}, angle {1} degrees, dot {2}, complete angle {3}".format(xval, degrees, dot, angle_complete))
	return (angle_complete, getTurn(xval))
	

def allTurnsEqual(dirs):
	next_dirs = cg.rotateForward(dirs)
	dirpairs = zip(dirs, next_dirs)
	turns = [computeTurn(curr,next) for (curr, next) in dirpairs]

	# total angles
	angle_sum = sum([t[0] for t in turns])
	#print ("*total angle {0}".format(angle_sum))
	large_epsilon = 0.9
	angle_sum_onecircle = math.fabs(angle_sum) < (360 + large_epsilon)

	# all turns must point to the same direction
	first = turns[0][1]
	#print ("**checking that all turns are {0}".format(first))
	all_equal = [t[1] == first for t in turns]
	all_turns_equal = all(all_equal)
	return all_turns_equal and angle_sum_onecircle

def isConvex(geo):
	positions = cg.getXZProjection(geo.points())
	dirs = cg.getDirections(positions)
	dirs_norm = map(lambda d: d.normalized(), dirs)
	#print ("getDummy(vecs) -> {0} ".format(getDummy(positions)))

	return allTurnsEqual(dirs_norm)
