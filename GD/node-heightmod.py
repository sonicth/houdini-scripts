import random
# Move Points Up
node = hou.pwd()
geo = node.geometry()

def absf(x):
    return x if x >= 0 else -x
    
def absv(v):
    return hou.Vector3(
    absf(v.x()),absf(v.y()),absf(v.z()) )
 
bb = geo.boundingBox()
sizevec = bb.maxvec() - bb.minvec()
halfvec = sizevec * 0.5
invhalf = halfvec / halfvec.lengthSquared()
cvec = ( bb.maxvec() + bb.minvec() ) * 0.5
m1 = hou.Matrix3()
m1.setToIdentity()
m1 *= 100
print ("min point {0} centre {1} inv {2}, abs {3}".
    format(absv(bb.minvec()), cvec, invhalf,
    absv(bb.minvec())) )
# Add code to modify contents of geo.
param_power = 1.962
param_height = 20
for point in geo.points():
    pos = point.position()
    p = (absv(pos - cvec)) / 50
    y = (1 - (p.x()**2+ p.z()**2)**param_power) * param_height
    pos += hou.Vector3(0, y, 0)
    point.setPosition(pos)

