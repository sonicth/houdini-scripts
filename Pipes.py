# Pipe Network
# example use:
# 
# import houdini_scripts.Pipes as ps
# reload(ps)
# p = ps.Piper()
# p.generate()

import hou
import math

def mix(t, a, b):
	if len(a) != len(b):
		raise "Vectors should have equal rank!"

	x = []
	for i in range(len(a)):
		value = a[i] * (1-t) + b[i] * t
		x.append(value)

	return x


class PathTracer:
	def __init__(self, start, end, steps = 20):
		self.start = start
		self.end = end
		self.steps = steps

	def sample(self, t):
		mixed = mix(t, self.start, self.end)

		theta = (t * math.pi * 2.0) * 5
		AMPLITUTE = 3.0
		mixed[2] = math.sin(theta) * AMPLITUTE
		return mixed

	def getCoords(self):
		point_strs = []
		for step in xrange(self.steps+1):
			#parameter
			t = step / float(self.steps)
			x,y,z = self.sample(t)
			point_txt = "{0},{1},{2}".format(x, y, z)
			point_strs.append(point_txt)
		return " ".join(point_strs)


class BendTracer(PathTracer):
	def __init__(self, start, end, steps = 20, bend_t = 0.4, bend_radius = 2):
		PathTracer.__init__(self, start, end, steps)
		self.bend_t = bend_t
		self.second_bend_t = bend_t + (1 - bend_t) * 0.5
	
	def sample(self, t):
		if t < self.bend_t:
			mixed_x = mix(t, [self.start[0]], [self.end[0]])[0]
			fixed_z = self.start[2]
			print "first bend x ",mixed_x, " t ",t
			return [mixed_x, self.start[1], fixed_z]
		elif self.bend_t <= t and t < self.second_bend_t:
			fixed_x = mix(self.bend_t, [self.start[0]], [self.end[0]])[0]
			# re-parametrise t on (second_bend_t, 1) to u on (0, 1)
			u = (t - self.bend_t) / (self.second_bend_t - self.bend_t)
			mixed_z = mix(u, [self.start[2]], [self.end[2]])[0]
			middle_v = [fixed_x, self.start[1], mixed_z] 
			print middle_v
			return middle_v
		else: # t > self.second_bend_t
			# orthogonal part
			u = (t - self.second_bend_t) / (1 - self.second_bend_t)
			mixed_x = mix(u, [self.start[0]], [self.end[0]])[0]
			fixed_z = self.end[2]
			print "last bend x ",mixed_x, " t ",t
			return [mixed_x, self.start[1], fixed_z]


class Piper:
	# geo
	# base

	def __init__(self):
		# great geometry node
		#TODO optinally pass geo node
		self.geo = hou.node("obj").createNode("geo", "pipes")
		# self.tracer = PathTracer([-10, 0, 0], [10, 0, 0])
		self.pt_start = [-10, 0, 0]
		self.pt_end = [12, 0, 8]
		self.tracer = BendTracer(self.pt_start, self.pt_end)

	def createBase(self):
		self.base = self.geo.createNode("circle")
		# polygon type
		self.base.parm("type").set(1)

	def generate(self):
		self.createBase()

		# TODO add curve + curve points
		curve = self.geo.createNode("curve", "path")
		curve.parm("order").set(3)
		curve.parm("coords").set(self.tracer.getCoords())
		curve.parm("type").set(1)

		#NOTE sweep is better than polyextrude!
		# pex = self.geo.createNode('polyextrude')
		# pex.parm('dist').set('-0.1')
		# pex.parm('divs').set(32)
		# pex.parm('spinetype').set(2)
		# pex.parm('dist').set(100)
		# pex.parm('uselocalzscaleattrib').set(True)
		# pex.parm('localzscaleattrib').set("elevation")
		# set base and path curve
		# pex.setInput(0, self.base)
		# pex.setInput(1, curve)
		
		sw = self.geo.createNode('sweep')
		sw.setInput(0, curve)
		sw.setInput(1, self.base)

		self.addDebugFeatures()
		
	def addDebugFeatures(self):
		marker_tail = self.geo.createNode("sphere", "curve_tail")
		marker_head = self.geo.createNode("sphere", "curve_head")
		
		self.translate(marker_tail, self.pt_start)
		Piper.translate(marker_head, self.pt_end)

		marker_tail.parm("scale").set(2)
		marker_head.parm("scale").set(2)
		#TODO
		# *position tail at the origin
		# *get dimensions of the 'front' or the end of the extrusion
		# *position the head at the front

	#TODO make less redundant!!
	@staticmethod
	def translate(obj, pos):
		obj.parm("tx").set(pos[0])
		obj.parm("ty").set(pos[1])
		obj.parm("tz").set(pos[2])
