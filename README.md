# Houdini Scripts and Plugins

## How to use scripts

### Setting Scripts on new Houdini Installation

HOUDINI_USER_PREF_DIR is usually :%Documents%/houdini %VERSION%: on Windows. Open Houdini Command Line Tools console and run *hconfig* to find out.

Module scripts need to be placed into $HOUDINI_USER_PREF_DIR/python2.7libs/


### creating lines for linesweep algorithm

-in Houdini in Polygon tab set Top View (top right viewport first button > Set View)
-in Create tab select Curve
-draw line segments by clicking twice
	-don't click third time, instead press enter
	-press q aggain to add more segments
-select new notes and in Modify tab choose Combine
-attach Python node to resulted Merge Sop
![Python attached Merge node](docs/images/merge-python-nodes.png)


## Native Houdini Plugin

Basis testbed for all C++ code, emphasising on geometry.

See updates in [UPDATES.md](src/UPDATES.md)

