##an example application
#
#import SegmentIntersection
#reload(SegmentIntersection)
#node = hou.pwd()
#geo = node.geometry()
#if SegmentIntersection.exist(geo):
#    print("intersections exist! >:D")
#else:
#    print("NO intersections! :?")

import hou
import math
import inlinecpp
import ComputationalGeometry as cg

import importlib
importlib.reload(cg)


def db_outputSegments(segs):
	i = 0
	for s in segs:
		print("segment {1} {0}".format(s, i))
		if not s.comparable(0):
			print(" ***segment {0} is not comparable at 0".format(i))
			i+=1
			continue
		y = s.sampleAtX(0)
		print("\ty sample at x=0: {0}".format(y))
		i+=1

def fish(segs):
	print("###number of segments {0}".format(len(segs)))
	mod1 = inlinecpp.createLibrary(
		name="fish1lib",
		includes="""
			#include <UT/UT_String.h>
			#include <iostream>
			#include <cstdio>
			""",
		function_sources=[
			"""
			bool matches(const char  *s, const  char *p)
			{
				UT_String us(s);
				// catfishing
				printf("\\n\\t***catfishing: %d ***\\n\\n", UT_String{p}.countChar('i'));
				return us.multiMatch(p);
			}
			"""
		]
	)
	stri = "I like fishing"
	for pat in "fish", "cat", "feash*":
		print (repr(stri), " matches pattern ", repr(pat), ": ", mod1.matches(stri, pat))


def exist(geo):
	print (geo)
	pts = geo.points()
	prims = geo.prims()

	# list of segments
	segs = [cg.Segment2(cg.getXY2D(prim.points())) for prim in prims]
	#db_outputSegments(segs)

	#NOTE to test inline cpp uncomment:
	# fish(segs)
 
	if cg.segmentIntersections(segs):
		print ("segments intersect!")
		return True

	return False
