@echo off
set USERLIBS_PATH=%USERPROFILE%\Documents\houdini19.5\scripts\python
set CURR_DIR=%~dp0

if not exist "%USERLIBS_PATH%" (
	echo creating directory "%USERLIBS_PATH%"...
	mkdir "%USERLIBS_PATH%"
)

for %%F in (ComputationalGeometry.py ConvexTest.py SegmentIntersection.py) do (
	mklink "%USERLIBS_PATH%\%%F" "%CURR_DIR%\%%F"
)

pause