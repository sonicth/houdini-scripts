/*
* Copyright (c) 2018
*	Side Effects Software Inc.  All rights reserved.
*
* Redistribution and use of Houdini Development Kit samples in source and
* binary forms, with or without modification, are permitted provided that the
* following conditions are met:
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
* 2. The name of Side Effects Software may not be used to endorse or
*    promote products derived from this software without specific prior
*    written permission.
*
* THIS SOFTWARE IS PROVIDED BY SIDE EFFECTS SOFTWARE `AS IS' AND ANY EXPRESS
* OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN
* NO EVENT SHALL SIDE EFFECTS SOFTWARE BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
* OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
* LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
* NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
* EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*----------------------------------------------------------------------------
* The Park Generator Peel SOP
*/

#define SIMPLEZ

//#include "shared/logging.h"

#include <boost/filesystem/path.hpp>
//#include <boost/container/string.hpp>
//namespace bc = boost::container;
#include "SOP_Play.h"

// This is an automatically generated header file based on theDsFile, below,
// to provide SOP_PlayParms, an easy way to access parameter values from
// SOP_PlayVerb::cook with the correct type.
#include "SOP_Play.proto.h"


#include <GU/GU_Detail.h>
#include <GEO/GEO_PrimPoly.h>
#include <OP/OP_Operator.h>
#include <OP/OP_OperatorTable.h>
#include <PRM/PRM_Include.h>
#include <PRM/PRM_TemplateBuilder.h>
#include <UT/UT_DSOVersion.h>
#include <UT/UT_Interrupt.h>
#include <UT/UT_StringHolder.h>
#include <SYS/SYS_Math.h>
#include <limits.h>
#include <OP/OP_AutoLockInputs.h>
#include <SOP/SOP_Error.h>
#include "hou-geometry.h"
#include <glm/glm.hpp>
#ifdef SIMPLEZ
	#include "geometry-algss.h"
#else
#include "GeometryTools/src/fit_rectangle.h"
#endif //SIMPLEZ



using namespace HDK_MB;
using namespace std;

//
// Help is stored in a "wiki" style text file.  This text file should be copied
// to $HOUDINI_PATH/help/nodes/sop/pg-Disperser.txt
//
// See the sample_install.sh file for an example.
//

/// This is the internal name of the SOP type.
/// It isn't allowed to be the same as any other SOP's type name.
const UT_StringHolder SOP_Play::theSOPTypeName("hdk_mv_play"_sh);

/// newSopOperator is the hook that Houdini grabs from this dll
/// and invokes to register the SOP.  In this case, we add ourselves
/// to the specified operator table.
void
newSopOperator(OP_OperatorTable *table)
{
	table->addOperator(new OP_Operator(
		SOP_Play::theSOPTypeName,   // Internal name
		"Play#",                     // UI name
		SOP_Play::myConstructor,    // How to build the SOP
		SOP_Play::buildTemplates(), // My parameters
		0,                          // Min # of sources
		1,                          // Max # of sources
		nullptr,                    // Custom local variables (none)
		OP_FLAG_GENERATOR));        // Flag it as generator
}

/// This is a multi-line raw string specifying the parameter interface
/// for this SOP.
static const char *theDsFile = R"THEDSFILE(
{
    name        parameters
	// parm {
	// 	name	"rule_file"
	// 	label	"Rule File"
	// 	type	file
	// }
	parm {
        name    "algorithm"
		cppname "Algorithm"
        label   "Select algorithm or debug shape"
        type    integer
        default { "1" }
        range   { 1 5 }
    }

	/*parm {
		name	"show_samples"
		label	"show_samples"
		type	toggle
		default	{ "1" }
	}*/
	parm {
        name    "offset1"
        name    "Offset1"
        label   "offset 1"
        type    float
        default { "0.5" }
        range   { 0 2 }
    }
	parm {
        name    "offset2"
        name    "Offset2"
        label   "offset 2"
        type    float
        default { "0" }
        range   { 0 2 }
    }
	parm {
        name    "numfries"
		cppname "NumFries"
        label   "Number of Fries"
        type    integer
        default { "10" }
        range   { 3 30 }
    }
	parm {
        name    "outputtype"
		cppname "OutputType"
        label   "type of output"
        type    integer
        default { "1" }
        range   { 1 5 }
    }
	parm {
        name    "epsilon"
        name    "Epsilon"
        label   "epsilon"
        type    float
        default { "0.01" }
        range   { 0 1 }
    }
	parm {
        name    "ignorebottom"
        cppname "IgnoreBottom"
        label   "Ignore bottom half of input"
        type    toggle
        default { "on" }
	}
	parm {
        name    "numiterations"
		cppname "NumIterations"
        label   "Number of Iterations"
        type    integer
        default { "1" }
        range   { 1 10 }
    }
}
)THEDSFILE";

PRM_Template*
SOP_Play::buildTemplates()
{
	static PRM_TemplateBuilder templ("SOP_Play.cpp"_sh, theDsFile);
	static double fishFucker{ 1.0 };
	static float fishFuckerf{ 1.0f };
	return templ.templates();
}

//char const gen_dylibname[] = "LayoutGenDyLib.dll";


//boost::shared_ptr<boost::dll::shared_library> placementlib;
//typedef boost::shared_ptr<ILogs> Ilogs_p;
//Ilogs_p logs;

//inline boost::filesystem::path getDyLibPath(char const *dylib_name)
//{
//	//auto path = dll::program_location();
//	auto su_plugin_path = boost::dll::this_line_location();
//	auto dylib_path = su_plugin_path.parent_path() / dylib_name;
//
//	return dylib_path;
//}


class SOP_CloudTransformVerb : public SOP_NodeVerb
{
public:
	SOP_CloudTransformVerb()
	{
		cerr << "**cloudtransform verb ctor" << endl;
		//auto genlib_path = getDyLibPath(gen_dylibname);

		//placementlib = boost::make_shared<boost::dll::shared_library>(genlib_path);
		//logs = initLogging(*placementlib);
	}
	virtual ~SOP_CloudTransformVerb()
	{
		cerr << "**cloudtransform verb dtor" << endl;
	}

	virtual SOP_NodeParms *allocParms() const { return new SOP_PlayParms(); }
	virtual UT_StringHolder name() const { return SOP_Play::theSOPTypeName; }

	virtual CookMode cookMode(const SOP_NodeParms *parms) const { return COOK_GENERIC; }

	virtual void cook(const CookParms &cookparms) const;

	/// This static data member automatically registers
	/// this verb class at library load time.
	static const SOP_NodeVerb::Register<SOP_CloudTransformVerb> theVerb;
};

// The static member variable definition has to be outside the class definition.
// The declaration is inside the class.
const SOP_NodeVerb::Register<SOP_CloudTransformVerb> SOP_CloudTransformVerb::theVerb;

const SOP_NodeVerb *
SOP_Play::cookVerb() const
{
	return SOP_CloudTransformVerb::theVerb.get();
}



/// This is the function that does the actual work.
void
SOP_CloudTransformVerb::cook(const SOP_NodeVerb::CookParms &cookparms) const
{
	// Start the interrupt scope
	UT_AutoInterrupt boss("Applying Layout Generator");

	auto &&sopparms = cookparms.parms<SOP_PlayParms>();
	GU_Detail *detail = cookparms.gdh().gdpNC();

	auto cook_time = cookparms.getCookTime();
	OP_Context context(cook_time);
	SOP_Node* n = cookparms.getNode();


	//std::vector<UT_Vector3> input_points;
	try
	{
		// lock inputs
		OP_AutoLockInputs inputs(n);
		if (inputs.lockInput(0, context) >= UT_ERROR_ABORT) {
			std::cerr << "input cannot be obtained!" << std::endl;
			return;
		}

	
		
		// This destroys everything except the empty P and topology attributes.
		detail->clearAndDestroy();

#ifndef SIMPLEZ
		FriesParams friesParams;
		friesParams.numFries = sopparms.getNumFries();
		friesParams.offset1 = sopparms.getOffset1();
		friesParams.offset2 = sopparms.getOffset2();
		friesParams.outputType = sopparms.getOutputType();
		friesParams.ignoreBottomHalf = sopparms.getIgnoreBottom();
		
		auto epsilon_linear = sopparms.getEpsilon();
		constexpr int POWER = 10;
		friesParams.eps = epsilon_linear;

		if (friesParams.numFries > 0.1)
#else
		DebugParams params;
		params["outputType"] = sopparms.getOutputType();
		params["numIterations"] = sopparms.getNumIterations();
		
		
		if (true)
#endif
		{
			int num_points_out = 0;

			// get input context
			const GU_Detail* igdp = cookparms.inputGeo(0);

			auto NumPrim = igdp->getNumPrimitives();
			auto NumVertex = igdp->getNumVertices();
			auto NumPoint = igdp->getNumPoints();
			


			//if (NumPrim == 0)
			//{
			//	cerr << "expected one polygon, got " << NumPrim << endl;
			//	return;
			//}
			if (NumPoint < 2)
			{
				cerr << "expected at least two points" << NumPrim << endl;
				return;
			}

			if (boss.wasInterrupted())
				return;


			std::vector<Pts_t> collectedResults;
#ifdef SIMPLEZ
			auto allPts = readPoints(igdp);

			switch (sopparms.getAlgorithm())
			{
			case 1:
				collectedResults = quickHull(allPts, params);
				break;
			case 2:
				collectedResults = grahamScanHull(allPts, params);
				break;
			case 3:
				collectedResults = testShape();
				break;
			default:
				break;
			}
#else
			auto prims = readPrimitives(igdp);
			for (auto &p: prims)
			{
				//p = shrink(p, std::min(numFries, 3.0));
				auto results = processFries(p, friesParams);
				copy(begin(results), end(results), back_inserter(collectedResults));
			}
#endif //SIMPLEZ		

			// WRITE primitives
			HuGeometry geom{ detail, "group_fishes" };
			for (auto& p : collectedResults)
			{
				//auto pts = convertHouToPolyf(p);
				auto num_points_current = p.size();
				num_points_out += num_points_current;

				geom.addGeometry(p);
			}
			

#ifdef LOOGS
			// show messages
			if (logs)
			{
				auto error_msgs = logs->getMessages(ILogs::LOG_ERROR);
				for (auto& msg : error_msgs)
				{
					cerr << "**error**: " << msg << endl;
				}

				logs->clear();
			}
			else { cerr << "***logs empty!" << endl; }
#endif //LOOGS

			//auto destroy_generator_fun = placementlib->get<void(IParkGenerator *)>("destroyGenerator");
			//destroy_generator_fun(gen_ptr);

			if (num_points_out == 0)
			{
				// With the range restriction we have on the divisions, this
				// is actually impossible, (except via integer overflow),
				// but it shows how to add an error message or warning to the SOP.
				cookparms.sopAddWarning(SOP_MESSAGE, "No points generated!");
			}
		}
	}
	catch (std::exception const&e)
	{
		cookparms.sopAddWarning(SOP_MESSAGE, e.what());
	}

}
