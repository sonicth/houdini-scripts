***
### 2022/07/16
TODO Interesting topics to read
* Markdown: https://www.markdownguide.org/basic-syntax/
* Read HDK in general https://www.sidefx.com/docs/hdk/index.html


### 2022/07/13
* `Debug` does not link because of the missing Debug-specific symbols symbols
* `RelWithDebInfo` did not compile because of the missing `ingore` symbol, a static variable, which was used when `UT_ASSERT_LEVEL` is greater than zero (`UT_ASSERT_LEVEL_NONE`). To fix this, I had to modify `HoudiniConfig.cmake` in `${HFS}/toolkit/cmake`, changing the assert level for Release with Debug info to 0.
