#include <iostream>
#include "../extern/happly/happly.h"
#include <vector>
#include <iterator>
#include <numeric>
#include <algorithm>
//TODOs
//1. import polygon
//2. write polygon
//3. split polygon


struct MeshData
{
	std::vector<std::array<double, 3>> vertices;
	std::vector<std::vector<size_t>> faceIndices;
};

using PolygonVertex = std::array<double, 3>;
std::ostream& operator<< (std::ostream& os, PolygonVertex const& v)
{
	os << "v: {" << v[0] << ", " << v[1] << ", " << v[2] << "}";
	return os;
}

struct Polygon
{
	std::vector<PolygonVertex> vertices;
};

Polygon readPly(std::string const &fname)
{
	constexpr bool READ_ONE_POLY = true;

	// Construct a data object by reading from file
	happly::PLYData plyIn(fname);

	auto vxPoss = plyIn.getVertexPositions();
	auto faceIdxs = plyIn.getFaceIndices<size_t>();
	
	Polygon p;

	if (!faceIdxs.empty())
	{
		if (READ_ONE_POLY)
		{
			p.vertices.reserve(vxPoss.size());
			copy(begin(vxPoss), end(vxPoss), back_inserter(p.vertices));
		}
		else
			// get first polygon
			for (auto& idx : faceIdxs[0])
			{
				p.vertices.push_back(vxPoss[idx]);
			}

	}
	
	return p;
}

void writePly(std::vector<Polygon> const &ps, std::string const& fname)
{
	using namespace std;

	std::vector<PolygonVertex> vertices;
	//TODO colours
	std::vector<std::vector<size_t>> indices;

	// push data in
	for (auto &p: ps)
		std::copy(begin(p.vertices), end(p.vertices), back_inserter(vertices));
	//	indices for first poly
	indices.resize(ps.size());
	int i = 0;
	int vertexCount = 0;
	for (auto p : ps)
	{
		indices[i].resize(p.vertices.size());
		std::iota(begin(indices[i]), end(indices[i]), vertexCount);
		vertexCount += p.vertices.size();
		++i;
	}

	// add data
	happly::PLYData out;
	out.addVertexPositions(vertices);
	out.addFaceIndices(indices);

	// write to file
	out.write(fname, happly::DataFormat::ASCII);
}

void writePly(Polygon const&p, std::string const& fname)
{
	using namespace std;

	std::vector<PolygonVertex> vertices;
	//TODO colours
	std::vector<std::vector<size_t>> indices;

	// push data in
	std::copy(begin(p.vertices), end(p.vertices), back_inserter(vertices));
	//	indices for first poly
	indices.resize(1);
	indices[0].resize(vertices.size());
	std::iota(begin(indices[0]), end(indices[0]), 0);

	// add data
	happly::PLYData out;
	out.addVertexPositions(vertices);
	out.addFaceIndices(indices);

	// write to file
	out.write(fname, happly::DataFormat::ASCII);
}


#include <boost/geometry.hpp>

namespace bg = boost::geometry;

using BgPoint = bg::model::d2::point_xy<float>;
using BgPolygon = bg::model::polygon<BgPoint>;
using BgSegment = bg::model::segment<BgPoint>;
using BgBox = bg::model::box<BgPoint>;

BgPolygon toBg(Polygon const &p)
{
	BgPolygon bgp;

	for (auto& v : p.vertices)
	{
		// NOTE negative z is new y
		bg::append(bgp, BgPoint{ (float)v[0], -(float)v[2] });
	}

	bg::correct(bgp);

	return bgp;
}

Polygon fromBg(BgPolygon const& bp)
{
	Polygon p;
	p.vertices.reserve(bp.outer().size() - 1);

	for (auto& v : bp.outer())
	{
		auto last = &*prev(bp.outer().end());
		if (&v != last) // omit the last one since the last one is the closing one
			// NOTE negative y ins negative z
			p.vertices.push_back({ v.x(), 0, -v.y() });
	}

	return p;
}

enum class EDirection { NEG_X, POS_X };

BgPolygon xtremePolygon(BgSegment const& seg, EDirection dir)
{
	constexpr decltype(BgPoint{}.x()) LARG_VAL = 200;
	BgPolygon p;

	switch (dir)
	{
	case EDirection::NEG_X:
		bg::append(p, seg.first);
		bg::append(p, seg.second); 
		bg::append(p, BgPoint{ -LARG_VAL, LARG_VAL });
		bg::append(p, BgPoint{ -LARG_VAL, -LARG_VAL });
		break;

	case EDirection::POS_X:
		bg::append(p, seg.second);
		bg::append(p, seg.first);
		bg::append(p, BgPoint{ LARG_VAL, -LARG_VAL });
		bg::append(p, BgPoint{ LARG_VAL, LARG_VAL });
		break;

	default:
		throw std::runtime_error("oops, wrong direction!");
	}

	bg::correct(p);
	
	return p;
}

void intersectTest(Polygon p)
{
	auto bp = toBg(p);

	BgSegment seg{ BgPoint{0, -100} , BgPoint{0, 100} };
	std::vector<BgPolygon> out;

	auto cuttingPoly = xtremePolygon(seg, EDirection::NEG_X);
	bg::intersection(bp, cuttingPoly, out);

}
template <typename Tfloat>
bool almostEqual(Tfloat a, Tfloat b)
{
	return abs(a - b) < 0.001;
}

class Sampler
{
	BgBox _box;
	float _fryWidth;
	int _count = 0;
public:
	Sampler(BgBox const&box, int numFries = 10)
		: _box{ box }
		, _fryWidth{ width() / std::min(numFries, 100) }
	{}

	float width() const { return _box.max_corner().x() - _box.min_corner().x(); }
	float length() const { return _box.max_corner().y() - _box.min_corner().y(); }

	bool atEnd() const
	{
		return almostEqual(_count * _fryWidth, width());
	}

	Sampler & operator++ ()
	{
		++_count;
		return *this;
	}

	BgPolygon operator* () const
	{
		auto maxc = _box.max_corner();
		auto minc = _box.min_corner();
		float offset = _fryWidth * _count;
		bool isLastFry = almostEqual((_count+1) * _fryWidth, width());
		BgBox currentBox{ 
			{minc.x() + offset, minc.y()}, 
			{isLastFry ? maxc.x() : minc.x() + offset + _fryWidth, maxc.y()} 
		};

		BgPolygon p;
		bg::convert(currentBox, p);
		return p;
	}
};


std::vector<Polygon> processFries(Polygon p, int numFries = 10)
{
	auto bp = toBg(p);

	// get bounding box
	BgBox aabb;
	bg::envelope(bp, aabb);

	std::vector<BgPolygon> out;

	for (Sampler s{ aabb, numFries }; !s.atEnd(); ++s)
	{
		bg::intersection(bp, *s, out);
	}
	
	std::vector<Polygon> ps;
	std::transform(begin(out), end(out), std::back_inserter(ps), [](auto &bgp) { return fromBg(bgp); });
	
	return ps;
}

Polygon process(Polygon p)
{
	auto bp = toBg(p);

	// get bounding box
	BgBox aabb;
	bg::envelope(bp, aabb);


	// clip it a bit
	auto cmin = aabb.min_corner();
	auto &cmax = aabb.max_corner();
	auto length = cmax.y() - cmin.y();
	auto clipLength = length * 0.20;
	cmin.y(cmin.y() + clipLength);
	BgBox aabbAdjusted{ cmin, cmax };

	BgPolygon aabbPoly;
	bg::convert(aabbAdjusted, aabbPoly);

	std::vector<BgPolygon> out;
	bg::intersection(bp, aabbPoly, out);

	auto pickBestPolygon = [](auto polygons) -> BgPolygon {
		//TODO pick one with the bigest area

		// easiest: first one! :D
		return polygons[0];
	};

	return fromBg(pickBestPolygon(out));
}


int main() 
{
	using namespace std;

	auto poly = readPly("../examples/poly01.ply");
	for (auto v : poly.vertices)
	{
		cout << "poly vertex " << v << endl;
	}
	//writePly(poly, "../output/fish01.ply");
	
	//poly = process(poly);
	//writePly(poly, "../output/fish04_front_clipped.ply");

	auto polys = processFries(poly, 10);
	writePly(polys, "../output/fish05_fries.ply");
	
	using namespace std; 
	cout << "fishing rectangles\n"; 

	




	return 0;
}
