#pragma once

#include "geometry-algss.h"

#include <string>
#include <glm/glm.hpp>
#include <algorithm>
#include <numeric>
#include <stdexcept>
#include <deque>

using namespace std;

//#include <array>
//using Line = std::array<glm::dvec2, 2>;
//using Line = Pts_t;
//
//

static inline void _checkEmpty(Pts_t const &pts)
{
	if (pts.empty())
		throw std::runtime_error("Points set is empty!");
}

static std::vector<Pts_t> _linesFromPts(Pts_t const &pts)
{
	std::vector<Pts_t> out;
	out.reserve(pts.size() / 2);

	// lower by two
	auto numPts = (pts.size() / 2) * 2;
	for (int i = 0; i < numPts; i += 2)
	{

		Pts_t line{ pts[i], pts[i + 1] };

		out.push_back(line);
	}


	if (pts.size() - numPts != 0)
	{
		// for odd count, last input point forms a triangle with the last output segment
		assert(pts.size() - numPts == 1);

		out.back().push_back(pts.back());
	}

	return out;
}

typename Pt_t::value_type cross2(Pt_t const& a, Pt_t const& b)
{
	return a.x * b.y - b.x * a.y;
}

std::vector<Pts_t> testShape()
{
	Pts_t pts{
		{1, 0},
		{0, 1},
		{1, 1},
	};
	return { pts };
}

std::vector<Pts_t> edgesFromPts(Pts_t const &pts)
{
	auto n = pts.size();
	vector<Pts_t> edges; edges.reserve(n);

	// NOTE returns empty set when input is a single point or an empty set 
	for (auto i = 0; i < n-1; ++n)
	{
		edges.push_back({ pts[i], pts[i + 1] });
	}
	return edges;
}

std::vector<Pts_t> quickHull(Pts_t const &pts, DebugParams params)
{
	std::vector<Pts_t> out;

	auto xAxisComp = [](auto pt1, auto pt2) { return pt1.x < pt2.x; };
	auto linePair = minmax_element(begin(pts), end(pts), xAxisComp);
	int aFirst = (int) std::distance(begin(pts), linePair.first);
	int bFirst = (int) std::distance(begin(pts), linePair.second);

	if (params["outputType"] == 1)
	{
		out.push_back({ pts[aFirst], pts[bFirst] });
		return out;
	}

	// function that finds local peak, the peak points as well as picked points
	int peak;
	Indices picked;
	Indices ptsIdxs; ptsIdxs.reserve(pts.size());
	for (int i = 0; i < pts.size(); ++i)
	{
		if (i != aFirst && i != bFirst)
			ptsIdxs.push_back(i);
	}

	struct QuickHelper
	{
		Pts_t const &pts;

		auto findPeak(int a, int b, Indices inputIdxs) -> std::pair<int, Indices>
		{
			int peak = -1;
			Indices picked;
			picked.reserve(inputIdxs.size() * 3 / 5); // slightly larger than half

			// normal to line
			Pt_t lineDir = pts[b] - pts[a];
			Pt_t n{ -lineDir.y, lineDir.x };

			double distMax = -1;
			for (int i = 0; i < inputIdxs.size(); ++i)
			{
				auto ptIdx = inputIdxs[i];
				auto pLocal = pts[ptIdx] - pts[a];

				auto distToLine = dot(n, pLocal);

				if (distToLine > 0) //NOTE or >=?
				{
					if (distToLine > distMax)
					{
						if (distMax >= 0) // not for the first time!
						{
							// save the previously picked point
							picked.push_back(peak);
						}
						
						// choose a new peak
						distMax = distToLine;
						peak = ptIdx;
					}
					else
						picked.push_back(ptIdx);
				}
			}

			return { peak, picked };
		};

		void addPoints(Indices &ptsOut, int a, int b, Indices const &ptsIn)
		{
			if (ptsIn.empty())
				return;

			auto [peak, picked] = findPeak(a, b, ptsIn);

			// add first part of the points
			addPoints(ptsOut, a, peak, picked);

			// add current point
			if (peak >= 0)
				ptsOut.push_back(peak);

			// add last part of the points
			addPoints(ptsOut, peak, b, picked);
		}
	};
	
	QuickHelper helper{pts};
	
	if (params["outputType"] == 2 || params["outputType"] == 3)
	{
		if (params["outputType"] == 2)
		{
			tie(peak, picked) = helper.findPeak(aFirst, bFirst, ptsIdxs);
		}
		else if (params["outputType"] == 3)
		{
			tie(peak, picked) = helper.findPeak(bFirst, aFirst, ptsIdxs);
		}

		if (!picked.empty())
		{
			// store points as primitives
			for (auto &i : picked)
				out.emplace_back(Pts_t{ pts[i] });

			// store line segments
			out.emplace_back(Pts_t{ pts[aFirst], pts[peak] });
			out.emplace_back(Pts_t{ pts[peak], pts[bFirst] });
			return out;
		}
	}

	// compute indices recursively
	Indices resultIndices;
	resultIndices.push_back(aFirst);
	helper.addPoints(resultIndices, aFirst, bFirst, ptsIdxs);
	resultIndices.push_back(bFirst);
	helper.addPoints(resultIndices, bFirst, aFirst, ptsIdxs);

	// add hull indices to output as a polygon
	Pts_t hull;
	hull.reserve(resultIndices.size());
	for (auto idx: resultIndices)
	{
		hull.push_back(pts[idx]);
	}
	out.push_back(hull);
	
	return out;
}

std::vector<Pts_t> grahamScanHull(Pts_t const &ptsIn, DebugParams params)
{
	if (ptsIn.empty())
		throw runtime_error("empty input geometry!");

	auto pts = ptsIn;
	auto n = pts.size();
	
	{
		// lowest y point
		auto  p0It = std::min_element(begin(pts), end(pts),	
			[](auto& a, auto& b) { return a.y != b.y 
				? a.y < b.y 
				// pick leftmost, e.g. min x, point
				: a.x < b.x; });

		// move p0 to front
		swap(*begin(pts), *p0It);
	}

	vector<typename Pt_t::value_type> xs; xs.reserve(pts.size());
	xs.push_back(0); // ignore p0
	transform(begin(pts) + 1, end(pts), back_inserter(xs), 
		[p0 = pts.front(), up = Pt_t{0, 1}](auto& pt) {
			auto nonunit = pt - p0; 
			auto unitDir = normalize(nonunit);
			return cross2(unitDir, up);
		});

	// use indices to sort points by directions with respect to p0 (including p0 it self)
	Indices sortedIdxs(n);
	iota(begin(sortedIdxs), end(sortedIdxs), 0);

	// sort indices after the first point
	sort(begin(sortedIdxs)+1, end(sortedIdxs), [&xs](auto idx1, auto idx2) { return xs[idx1] < xs[idx2]; });



	/// angle formed by 
	auto isReflex = [&](int ai, int bi, int ci)
	{
		Pt_t &a = pts[sortedIdxs[ai]],
			 &b = pts[sortedIdxs[bi]],
			 &c = pts[sortedIdxs[ci]];

		auto d1 = b - a;
		auto d2 = c - b;
		auto z = cross2(d1, d2);
		return z > 0;
	};

	// start hullstack
	deque<int> hullStack;

	// first two points (p0 and first sorted point) are in the hull already
	hullStack.push_front(0);
	hullStack.push_front(1);

	int i = 2;
	while (i < n)
	{
		while (isReflex(hullStack[1], hullStack[0], i))
		{
			assert(i > 2);

			hullStack.pop_front();
		}

		hullStack.push_front(i);

		++i;
	}

	Pts_t hull; hull.reserve(hullStack.size());

	transform(rbegin(hullStack), rend(hullStack), back_inserter(hull), 
		[&pts, &sortedIdxs](int idx) 
		{
			return pts[sortedIdxs[idx]];
		});

	return {hull};
}

