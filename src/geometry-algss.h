#pragma once

#include <vector>
#include <glm/fwd.hpp>

using Pt_t = glm::dvec2;
using Pts_t = std::vector<Pt_t>;
using Indices = std::vector<int>;

#include <unordered_map>
#include <string>
using DebugParams = std::unordered_map<std::string, float>;

std::vector<Pts_t> quickHull(Pts_t const& pts, DebugParams params = {});
std::vector<Pts_t> grahamScanHull(Pts_t const &pts, DebugParams params = {});

// utilities
std::vector<Pts_t> edgesFromPts(Pts_t const &pts);

std::vector<Pts_t> testShape();
