#include "hou-attribs.h"
#include "houdini-common-include.h"

#include <GU/GU_Detail.h>
#include <iostream>
#include <GOP/GOP_Manager.h>


using namespace std;


void Error(std::string const &str)
{
	cerr << "Error: " << str << endl;
	throw std::runtime_error(str);
}

template <typename Tcond>
void ErrorUnless(Tcond cond, std::string const &str)
{
	if (!cond)
		Error(str);
}

int getMeSome()
{
	return -200;
}

//void getCloud(GU_Detail const *detail_input, PointCloudSet &cloud_incomming)
//{
//	GA_Attribute const *attrib_cd = detail_input->findFloatTuple(GA_ATTRIB_POINT, "Cd", 3);
//	GA_Attribute const *attrib_p = detail_input->findFloatTuple(GA_ATTRIB_POINT, "P", 3);
//	const GA_AIFTuple *tuple_cd = attrib_cd->getAIFTuple();
//	const GA_AIFTuple *tuple_p = attrib_p->getAIFTuple();
//	if (tuple_cd && tuple_p)
//	{
//		UT_Vector3 Cd;
//		UT_Vector3 p;
//		for (GA_Iterator it(detail_input->getPointRange()); !it.atEnd(); it.advance())
//		{
//			GA_Offset offset = it.getOffset();
//
//			tuple_cd->get(attrib_cd, offset, Cd.data(), 3);
//			tuple_p->get(attrib_p, offset, p.data(), 3);
//
//			cloud_incomming << CloudPoint{ {p.x(), p.y(), p.z() }, {Cd.x(), Cd.y(), Cd.z()} };
//			//input_points.push_back(p);
//			//input_colours.push_back(Cd);
//		}
//	}
//
//}
//
//struct CloudWriter::Pi
//{
//	GU_Detail *detail_output;
//	int total_point_count;
//	int current_point_count;
//	bool squish_colour_to_01;
//	GA_Attribute *attrib_p;
//	GA_Attribute *attrib_cd;
//	GA_AIFTuple const *tuple_cd;
//	GA_AIFTuple const *tuple_p;
//	GA_Iterator it;
//};
//
//CloudWriter::CloudWriter(GU_Detail* detail_output_, int num_points, bool squish_colour_to_01_)
//	: pi{ new Pi }
//{
//	// Add diffuse color, if not already added on previous cook.
///*GA_RWHandleV3 colorh(detail_output->findDiffuseAttribute(GA_ATTRIB_POINT));
//if (!colorh.isValid())
//	colorh = GA_RWHandleV3(detail_output->addDiffuseAttribute(GA_ATTRIB_POINT));
//
//*/
//
//	pi->detail_output = detail_output_;
//
//	pi->total_point_count = num_points;
//	pi->current_point_count = 0;
//
//	pi->squish_colour_to_01 = squish_colour_to_01_;
//
//	pi->attrib_cd = pi->detail_output->findFloatTuple(GA_ATTRIB_POINT, "Cd", 3);
//	if (!pi->attrib_cd)
//		pi->attrib_cd = pi->detail_output->addFloatTuple(GA_ATTRIB_POINT, "Cd", 3);
//
//	pi->attrib_p = pi->detail_output->findFloatTuple(GA_ATTRIB_POINT, "P", 3);
//	pi->tuple_cd = pi->attrib_cd->getAIFTuple();
//	pi->tuple_p = pi->attrib_p->getAIFTuple();
//
//	ErrorUnless(pi->tuple_cd && pi->tuple_p, "Could not initialise/obtain point and colour attributes!");
//
//	pi->it = GA_Iterator(pi->detail_output->getPointRange());
//}
//
//CloudWriter::~CloudWriter()
//{
//	delete pi;
//}
//
//void CloudWriter::store(PointCloudSet const& cloud_outgoing, std::string const& group_name)
//{
//	auto current_size = cloud_outgoing.size();
//	cerr << "*CloudWriter: storing points from " << pi->current_point_count << " to " << (pi->current_point_count + current_size) << endl;
//	ErrorUnless(current_size + pi->current_point_count <= pi->total_point_count, "Trying to store more points than declared!");
//
//	GA_Offset offset_current, offset_start = pi->it.getOffset();
//
//	for (int i = 0;
//		i < current_size && !pi->it.atEnd();
//		++i, pi->it.advance())
//	{
//		// 1. prepare points
//		auto &point_p = cloud_outgoing[i].getPosition();
//		auto &point_cd = cloud_outgoing[i].getColour();
//		// convert and write to output detail attributes
//		UT_Vector3 p{ point_p.mX, point_p.mY, point_p.mZ };
//		UT_Vector3 Cd{ point_cd.mX, point_cd.mY, point_cd.mZ };
//		// resize colour range [0..255] -> [0..1]
//		if (pi->squish_colour_to_01)
//		{
//			Cd /= 255.0;
//		}
//
//		// 2. store in the attribute!
//		offset_current = pi->it.getOffset();
//		pi->tuple_cd->set(pi->attrib_p, offset_current, p.data(), 3);
//		pi->tuple_cd->set(pi->attrib_cd, offset_current, Cd.data(), 3);
//
//	}
//
//
//	/*GA_PointGroup *group = nullptr;*/
//	if (group_name != "")
//	{
//		// create new group
//		auto group = pi->detail_output->newPointGroup(group_name);
//
//		// get point index map
//		auto &index_map = pi->detail_output->getPointMap();
//
//		// add range of points
//		GA_Range point_range{ index_map, offset_start, offset_current };
//		cerr << "**adding group [" << group_name << "] on the range from " << offset_start << ", to " << offset_current << " (inclusive)" << endl;
//		group->addRange(point_range);
//		//group->addOffset(GA_Offset{});
//		//group->addIndex(GA_Index{});
//
//		//pi->detail_output->newDetachedPointGroup()
//		//GOP_Manager m;
//		//m.createPointGroup()
//		//m.parseOrderedPointDetached()
//		//getPointOffset
//
//	}
//
//	pi->current_point_count += current_size;
//}


//////////////////////
StringStorer::StringStorer(
	GU_Detail * detail,
	std::string const & attrib_name,
	int attrib_owner)
	// create attribute
	: attrib_ptr{ new GA_RWHandleS(detail->addStringTuple((GA_AttributeOwner)attrib_owner, attrib_name, 1)) }
{
	auto &str_attrib = *(GA_RWHandleS *)attrib_ptr;
	ErrorUnless(str_attrib.isValid(), "Could not create attribute " + attrib_name);
}

StringStorer::~StringStorer()
{
	delete (GA_RWHandleS *)attrib_ptr;
}

void StringStorer::operator() (std::string const & value, GA_Offset offset)
{
	auto &str_attrib = *(GA_RWHandleS *)attrib_ptr;
	// set!
	str_attrib.set(GA_Offset(offset), value);
}


FloatStorer::FloatStorer(GU_Detail * detail, std::string const & attrib_name, int attrib_owner)	
	// create attribute
	: attrib_ptr{ new GA_RWHandleF(detail->addFloatTuple((GA_AttributeOwner)attrib_owner, attrib_name, 1)) }
{
	auto &str_attrib = *(GA_RWHandleF *)attrib_ptr;
	ErrorUnless(str_attrib.isValid(), "Could not create attribute " + attrib_name);
}

FloatStorer::~FloatStorer()
{
	delete (GA_RWHandleF *)attrib_ptr;
}

void FloatStorer::operator()(value_type const & value, GA_Offset offset)
{
	auto &attrib = *(GA_RWHandleF *)attrib_ptr;
	// set!
	attrib.set(GA_Offset(offset), value);
}


////////////////////

void storeArrayData(GU_Detail* detail, std::string const& attrib_name, const UT_FloatArray& data)
{
	auto attrib = detail->addFloatArray(GA_ATTRIB_GLOBAL, attrib_name, 1);
	ErrorUnless(attrib, "Failed to create " + attrib_name + " attribute!");

	auto aif = attrib->getAIFNumericArray();
	ErrorUnless(aif, attrib_name + " attribute is not a numeric array!");

	aif->set(attrib, GA_Offset(0), data);

	attrib->bumpDataId();
}

void getArrayData(GU_Detail const * detail, std::string const& attrib_name, UT_FloatArray& data_out)
{
	// find attribute
	auto attrib = detail->findFloatArray(GA_ATTRIB_GLOBAL, "camera_parameters", -1, -1);
	ErrorUnless(attrib, attrib_name + " attribute is not found!");

	// get array reference
	auto aif = attrib->getAIFNumericArray();
	ErrorUnless(aif, "" + attrib_name + " attribute is not a numeric array!");

	// get data
	aif->get(attrib, GA_Offset(0), data_out);
}

void storeString(GU_Detail * detail, std::string const & attrib_name, std::string const & value)
{
	// create attribute
	GA_RWHandleS str_attrib(detail->addStringTuple(GA_ATTRIB_GLOBAL, attrib_name, 1));
	ErrorUnless(str_attrib.isValid(), "Could not create attribute " + attrib_name);

	// set!
	str_attrib.set(GA_Offset(0), value);
}

std::string getString(GU_Detail const* detail, std::string const & attrib_name)
{
	GA_ROHandleS str_attrib(detail->findStringTuple(GA_ATTRIB_GLOBAL, attrib_name, 1));
	ErrorUnless(str_attrib.isValid(), "Could not get attribute " + attrib_name);

	auto str = str_attrib.get(GA_Offset(0));
	return std::string(str.begin(), str.end());
}

void storeInt(GU_Detail * detail, std::string const & attrib_name, int value)
{
	// create attribute
	GA_RWHandleI int_attrib(detail->addIntTuple(GA_ATTRIB_GLOBAL, attrib_name, 1));
	ErrorUnless(int_attrib.isValid(), "Could not create attribute " + attrib_name);

	// set!
	int_attrib.set(GA_Offset(0), value);
}

int getInt(GU_Detail const* detail, std::string const & attrib_name)
{
	GA_ROHandleI int_attrib(detail->findIntTuple(GA_ATTRIB_GLOBAL, attrib_name, 1));
	ErrorUnless(int_attrib.isValid(), "Could not get attribute " + attrib_name);

	int val = int_attrib.get(GA_Offset(0));
	return val;
}

void storeFloatA(GU_Detail * detail, std::string const & attrib_name, std::vector<float> input)
{
	// create attribute
	GA_RWHandleFA fa_attrib(detail->addFloatArray(GA_ATTRIB_GLOBAL, attrib_name, 1));
	ErrorUnless(fa_attrib.isValid(), "Could not create attribute " + attrib_name);

	// copy vector into array
	UT_FloatArray floata((exint)input.size());
	for (auto val : input)
	{
		floata.append(val);
	}

	// set attribute array!
	fa_attrib.set(GA_Offset(0), floata);
}

std::vector<float> getFloatA(GU_Detail const* detail, std::string const & attrib_name)
{
	GA_ROHandleFA fa_attrib(detail->findFloatArray(GA_ATTRIB_GLOBAL, attrib_name, 1));
	ErrorUnless(fa_attrib.isValid(), "Could not get attribute " + attrib_name);

	// get attribute array!
	UT_FloatArray floata;
	fa_attrib.get(GA_Offset(0), floata);

	// copy into vector; many ways to do this!
	std::vector<float> output(floata.size());
	copy(floata.data(), floata.data() + floata.size(), output.begin());

	return output;
}
