#pragma once
//TODO remove this include dependency:
#include <string>


//#include <UT/UT_VectorTypes.h>
#include <GA/GA_Types.h>

// OR
template <typename T> class  UT_ValArray;
typedef UT_ValArray<float>	 UT_FloatArray;

class GU_Detail;
class PointCloudSet;

int getMeSome();

template <typename T>
class StorerBase
{
protected:
	using value_type = T;
public:

	virtual ~StorerBase() {}
	virtual void operator() (value_type const &value, GA_Offset offset) = 0;
};

class StringStorer
	: public StorerBase<std::string>
{
	void *attrib_ptr;
public:
	StringStorer(
		GU_Detail * detail,
		std::string const & attrib_name,
		int attrib_owner = GA_ATTRIB_GLOBAL);

	~StringStorer();

	void operator() (value_type const & value, GA_Offset offset = 0) override;
};

class FloatStorer
	: public StorerBase<float>
{
	void *attrib_ptr;
public:
	FloatStorer(
		GU_Detail * detail,
		std::string const & attrib_name,
		int attrib_owner = GA_ATTRIB_GLOBAL);

	FloatStorer() = delete;
	FloatStorer(FloatStorer const &) = delete;
	FloatStorer(FloatStorer &&f) : attrib_ptr{ f.attrib_ptr } {}

	~FloatStorer();

	void operator() (value_type const & value, GA_Offset offset = 0) override;
};

void storeArrayData(GU_Detail* detail, std::string const& attrib_name, const UT_FloatArray& meta_data);
void getArrayData(GU_Detail const * detail, std::string const& attrib_name, UT_FloatArray& data_out);

void storeString(GU_Detail* detail, std::string const& attrib_name, std::string const& value);
std::string getString(GU_Detail const* detail, std::string const& attrib_name);

void storeInt(GU_Detail * detail, std::string const & attrib_name, int value);
int getInt(GU_Detail const* detail, std::string const & attrib_name);

#include <vector>

void storeFloatA(GU_Detail * detail, std::string const & attrib_name, std::vector<float> input);
std::vector<float> getFloatA(GU_Detail const* detail, std::string const & attrib_name);
