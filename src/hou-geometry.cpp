#include "houdini-common-include.h"
#include "hou-geometry.h"
#include "hou-attribs.h"
#include <glm/glm.hpp>
#include <UT/UT_Interrupt.h>

using namespace std;

std::ostream & operator<< (std::ostream &os, glm::dvec2 pt)
{
	os << "{dvec2: " << pt.x << ", " << pt.y << "}";
	return os;
}



/// @param(GU_Detail_*) detail_							plugin current output context
/// @param(std::string const&) primitive_group_name		name for the primitive (e.g a polygon) group
HuGeometry::HuGeometry(GU_Detail* detail_, std::string const& primitive_group_name)
	: detail{ detail_ }
{
	prim_group_quarters = detail->newPrimitiveGroup(primitive_group_name);
	if (!prim_group_quarters) {
		cerr << "ERROR****could not create quarter group";
		throw std::runtime_error("could not create primitive group: " + primitive_group_name);
	}
}

void HuGeometry::addGeometry(Pts_t const& poly_points)
{
	auto num_vertices_current = poly_points.size();

	// offsets for each shape polygon
	GA_Offset start_vtxoff;

	// create polygon
	auto start_primoff = detail->appendPrimitivesAndVertices(GA_PRIMPOLY, 1, num_vertices_current, start_vtxoff, true);

	// add primitive group
	//NOTE uncomment when primitive groups are not expected: 
	// if (prim_group_quarters)
	prim_group_quarters->addOffset(start_primoff);

	// add points
	auto start_ptoff = detail->appendPointBlock(num_vertices_current);

	// write points
	for (int point_idx = 0; point_idx < num_vertices_current; ++point_idx)
	{
		UT_Vector3 point{ (float)poly_points[point_idx].x, 0, -(float)poly_points[point_idx].y };
		detail->setPos3(start_ptoff + point_idx, point);
	}

	// wire points to vertices
	for (int point_idx = 0; point_idx < num_vertices_current; ++point_idx)
	{
		detail->getTopology().wireVertexPoint(start_vtxoff + point_idx, start_ptoff + point_idx);
	}

	// We added points, vertices, and primitives,
	// so this will bump all topology attribute data IDs,
	// P's data ID, and the primitive list data ID.
	detail->bumpDataIdsForAddOrRemove(true, true, true);

	num_points_total += num_vertices_current;
	num_polys_total++;
}

std::vector<Pts_t> readPrimitives(const GU_Detail* igdp)
{

	//int num_points_out = 0;
	//auto convertHouToPolyf = [](auto& points)
	//{
	//	Pts_t polygon; polygon.reserve(points.size());
	//	for (auto& pt : points)
	//	{
	//		polygon.push_back({ (double)pt[0], (double)-pt[2] });
	//	}
	//	return polygon;
	//};


	// READ primitives
	auto num_primitives = igdp->getNumPrimitives();
	std::vector<Pts_t> polys(num_primitives);
	const GEO_Primitive* prim;
	int primi = 0;
	GA_FOR_ALL_PRIMITIVES(igdp, prim)
	{
		auto num_vertices = prim->getVertexCount();
		Pts_t poly(num_vertices);
		//prim->getPointRange()
		for (GA_Size i = 0; i < num_vertices; ++i)
		{
			auto pos = prim->getPos3(i);
			poly[i] = { (double)pos[0], -(double)pos[2] };
		}
		polys[primi++] = poly;
	}

	return polys;
}

Pts_t readPoints(const GU_Detail* igdp)
{
	auto num_points = igdp->getNumPoints();
	Pts_t pts; pts.reserve(num_points);

	// loop through points getting positions
	UT_Array<GA_RWHandleV3> positionattribs(1);
	GA_Attribute *attrib;
	GA_FOR_ALL_POINT_ATTRIBUTES(igdp, attrib)
	{
		// Skip non-transforming attributes
		if (!attrib->needsTransform())
			continue;

		GA_TypeInfo typeinfo = attrib->getTypeInfo();

		switch (typeinfo)
		{
		case GA_TypeInfo::GA_TYPE_POINT:
		case GA_TypeInfo::GA_TYPE_HPOINT:
			{
			//GA_ROHandleV3 handle(attrib);
			GA_RWHandleV3 handle(attrib);
				
				if (handle.isValid())
				{
					positionattribs.append(handle);
					attrib->bumpDataId();
				}
			}
		}
	}

	// Iterate over points up to GA_PAGE_SIZE at a time using blockAdvance.
	GA_Offset start;
	GA_Offset end;

	const GA_PointGroup *myGroup = nullptr;
	for (GA_Iterator it(igdp->getPointRange(myGroup)); it.blockAdvance(start, end);)
	{
		UT_AutoInterrupt progress("Reading point coordinates");
		
		// Check if user requested abort
		if (progress.wasInterrupted())
			break;

		for (GA_Offset ptoff = start; ptoff < end; ++ptoff)
		{
			for (exint i = 0; i < positionattribs.size(); ++i)
			{
				UT_Vector3 p = positionattribs(i).get(ptoff);
				pts.emplace_back(p.x(), -p.z());
			}
		}
	}
	return pts;
}

Pts_t shrink(Pts_t const& pts, float amount)
{
	using Pt = typename Pts_t::value_type;

	// average of points is centre
	Pt centre;
	for (auto pt : pts)
	{
		centre += pt;
	}
	centre /= pts.size();

	// scale by dinstance from the centre
	Pts_t out; out.reserve(pts.size());
	for (auto pt : pts)
	{
		pt = glm::mix(centre, pt, amount);
		out.push_back(pt);
	}

	return out;
}

Pts_t smashPts(std::vector<Pts_t> const& vpts)
{
	Pts_t out;
	auto outi = std::back_inserter(out);
	for (auto pts : vpts)
	{
		std::copy(begin(pts), end(pts), outi);
	}

	return out;
}
