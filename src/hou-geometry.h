#pragma once

#include <string>
#include <vector>
#include <glm/fwd.hpp>

using Pts_t = std::vector<glm::dvec2>;

// fdecl of houdini plugin class
class GU_Detail;

// a class that encapsultes GU_detail and friends privately, 
//  with an interface for adding (standalone, to begin with) polygons simply
class HuGeometry
{
	GU_Detail* detail;
	GA_PrimitiveGroup* prim_group_quarters = nullptr;
	GA_Size num_points_total = 0;
	GA_Size num_polys_total = 0;
public:
	HuGeometry(GU_Detail* detail_, std::string const& primitive_group_name = "shape__quarter");

	// add a standalone 2D Polygon in (X, Y) mapped into (X, -Z) 
	void addGeometry(Pts_t const&);
};

std::vector<Pts_t> readPrimitives(const GU_Detail* igdp);
Pts_t readPoints(const GU_Detail *igdp);

Pts_t smashPts(std::vector<Pts_t> const& vpts);

Pts_t shrink(Pts_t const& pts, float amount);


