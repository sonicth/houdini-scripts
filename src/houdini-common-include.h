#pragma once

//#define STRING2(x) #x
//#define STRING(x) STRING2(x)

#define PP_VAR 123
//#error "Value of PP_VAR = " PP_VAR
//#error "Value of PP_VAR = " #PP_VAR
//#error "Value of PP_VAR = " ##PP_VAR

#pragma message( "Compiling " __FILE__ )
#pragma message( "Last modified on " __TIMESTAMP__ )

#define STRING2(x) #x
#define STRING(x) STRING2(x)

//#include <boost/function.hpp>
#include <iostream>
#include <string>
#include <GA/GA_OffsetList.h>
#include <GA/GA_PrimitiveTypes.h>
#include <GU/GU_Detail.h>

//#include <boost/algorithm/string.hpp>
